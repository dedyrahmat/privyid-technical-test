# Notes
You may encounter **CORS** because the Server/API itself has not been configured to handle the **CORS** and/or not opened to Public Access


# Getting Started

#### Cloning this project

```bash
git clone https://gitlab.com/dedyrahmat/privyid-technical-test.git
```
#### Install dependecies
```bash
npm install
# or
yarn
```
#### Run Development Server
```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

# Deploy

#### Building Project
```bash
npm run build 
# or
yarn build
```

#### Starting Production Server
```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
