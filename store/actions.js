export const SET_USER = "SET_USER";
export const SET_ACCESS_TOKEN = "SET_ACCESS_TOKEN";
export const SET_PROFILE = "SET_PROFILE";
export const SET_LOADING = "SET_LOADING";
export const SET_ERROR = "SET_ERROR";
export const SET_ERROR_MESSAGE = "SET_ERROR_MESSAGE";
