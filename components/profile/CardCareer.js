import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import querystring from "querystring";
import { useSelector } from "react-redux";
import { axiosGlobal } from "../../helpers/global";

function CardCareer({ career }) {
  const [editMode, setEditMode] = useState(false);
  const accessToken = useSelector((state) => state.accessToken);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      position: null,
      company_name: career.company_name,
      starting_from: career.starting_from,
      ending_in: career.ending_in,
    },
    validationSchema: Yup.object({
      company_name: Yup.string().required(
        "Nama Perusahaan tidak boleh kosong."
      ),
      starting_from: Yup.date().required("Tanggal Mulai tidak boleh kosong."),
      ending_in: Yup.date().required("Tanggal Berakhir tidak boleh kosong."),
    }),
    onSubmit: (values) => updateCareer(values),
  });
  const updateCareer = (values) => {
    const body = querystring.stringify(values);
    const headers = {
      Authorization: accessToken,
    };
    axiosGlobal
      .post("/profile/career", body, { headers })
      .then((response) => {
        if (response.status === 201) {
          console.log("Career Updated");
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  return (
    <div className="bg-white shadow-md rounded border p-4">
      <form method="post" onSubmit={formik.handleSubmit}>
        <div className="flex justify-between items-center">
          <h1 className="text-xl font-semibold">Riwayat Karir</h1>
          <span
            onClick={() => setEditMode(!editMode)}
            className="material-icons bg-gray-300 cursor-pointer rounded-full p-2"
          >
            edit
          </span>
        </div>
        <hr className="my-3" />
        <div className="gap-y-1 grid mb-4">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Nama Perusahaan
          </label>
          {editMode ? (
            <>
              <input
                type="text"
                name="company_name"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.company_name}
                onChange={formik.handleChange}
              />
              {formik.errors.company_name && formik.touched.company_name && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.company_name}
                </p>
              )}
            </>
          ) : (
            <span>{career.company_name || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-4">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Tanggal Mulai
          </label>
          {editMode ? (
            <>
              <input
                type="date"
                name="starting_from"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.starting_from}
                onChange={formik.handleChange}
              />
              {formik.errors.starting_from && formik.touched.starting_from && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.starting_from}
                </p>
              )}
            </>
          ) : (
            <span>{career.starting_from || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Tanggal Berakhir
          </label>
          {editMode ? (
            <>
              <input
                type="date"
                name="ending_in"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.ending_in}
                onChange={formik.handleChange}
              />
              {formik.errors.ending_in && formik.touched.ending_in && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.ending_in}
                </p>
              )}
            </>
          ) : (
            <span>{career.ending_in || "Tidak tersedia."}</span>
          )}
        </div>
        {editMode && (
          <button
            type="submit"
            className="bg-blue-400 w-full font-bold outline-none text-white text-center py-2 rounded shadow cursor-pointer"
          >
            Simpan
          </button>
        )}
      </form>
    </div>
  );
}

export default CardCareer;
