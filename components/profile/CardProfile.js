import { useFormik } from "formik";
import * as Yup from "yup";
import querystring from "querystring";
import { useSelector, useDispatch } from "react-redux";
import { axiosGlobal } from "../../helpers/global";
import { useState } from "react";
import { setAccessToken } from "../../store/actionCreator";
import Router from "next/router";

function CardProfile({ user }) {
  const accessToken = useSelector((state) => state.accessToken);
  const [editMode, setEditMode] = useState(false);
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: user.name,
      birthday: user.birthday,
      gender: 99,
      hometown: user.hometown,
      bio: user.bio,
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Nama tidak boleh kosong."),
      birthday: Yup.date().required("Tanggal Lahir tidak boleh kosong."),
      gender: Yup.number().required("Jenis Kelamin tidak boleh kosong."),
      hometown: Yup.string().required("Alamat tidak boleh kosong."),
      bio: Yup.string().required("Bio tidak boleh kosong."),
    }),
    onSubmit: (values) => updateProfile(values),
  });

  const updateProfile = (values) => {
    const body = querystring.stringify(values);
    const headers = {
      Authorization: accessToken,
    };
    axiosGlobal
      .post("/profile", body, { headers })
      .then((response) => {
        if (response.status === 201) {
          console.log("Profile Updated");
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  const logout = () => {
    dispatch(setAccessToken(null));
    Router.replace("/");
  };
  return (
    <section className="col-span-4 bg-white shadow-md rounded border p-4">
      <form onSubmit={formik.handleSubmit}>
        <div className="flex justify-between items-center">
          <h1 className="text-xl font-semibold">Profile</h1>
          <span
            onClick={() => setEditMode(!editMode)}
            className="material-icons bg-gray-300 cursor-pointer rounded-full p-2"
          >
            edit
          </span>
        </div>
        <hr className="my-3" />
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Nama
          </label>
          {editMode ? (
            <>
              <input
                id="name"
                type="text"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.name}
                onChange={formik.handleChange}
              />
              {formik.errors.name && formik.touched.name && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.name}
                </p>
              )}
            </>
          ) : (
            <span>{user.name || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="age" className="font-bold text-sm text-gray-800">
            Umur
          </label>
          <span>{user.age || "Tidak tersedia."}</span>
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="birthday" className="font-bold text-sm text-gray-800">
            Tanggal Lahir
          </label>
          {editMode ? (
            <>
              <input
                id="birthday"
                type="date"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.birthday}
                onChange={formik.handleChange}
              />
              {formik.errors.birthday && formik.touched.birthday && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.birthday}
                </p>
              )}
            </>
          ) : (
            <span>{user.birthday || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="gender" className="font-bold text-sm text-gray-800">
            Jenis Kelamin
          </label>
          {editMode ? (
            <>
              <select
                id="gender"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.gender}
                onChange={formik.handleChange}
              >
                <option value={99}>Pilih Jenis Kelamin</option>
                <option value={0}>Pria</option>
                <option value={1}>Wanita</option>
              </select>
              {formik.errors.gender && formik.touched.gender && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.gender}
                </p>
              )}
            </>
          ) : (
            <span>
              {user.gender !== null
                ? user.gender === 0
                  ? "Pria"
                  : "Wanita"
                : "Tidak tersedia."}
            </span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="zodiac" className="font-bold text-sm text-gray-800">
            Zodiak
          </label>
          <span>{user.zodiac || "Tidak tersedia."}</span>
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="hometown" className="font-bold text-sm text-gray-800">
            Alamat
          </label>
          {editMode ? (
            <>
              <textarea
                id="hometown"
                type="text"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.hometown}
                onChange={formik.handleChange}
              />
              {formik.errors.hometown && formik.touched.hometown && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.hometown}
                </p>
              )}
            </>
          ) : (
            <span>{user.hometown || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="bio" className="font-bold text-sm text-gray-800">
            Bio
          </label>
          {editMode ? (
            <>
              <textarea
                id="bio"
                type="text"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.bio}
                onChange={formik.handleChange}
              />
              {formik.errors.bio && formik.touched.bio && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.bio}
                </p>
              )}
            </>
          ) : (
            <span>{user.bio || "Tidak tersedia."}</span>
          )}
        </div>
        {editMode && (
          <button
            type="submit"
            className="bg-blue-400 w-full font-bold outline-none text-white text-center py-2 rounded shadow cursor-pointer"
          >
            Update Profile
          </button>
        )}
        <button
          type="button"
          onClick={logout}
          className="bg-red-400 w-full font-bold outline-none mt-3 text-white text-center py-2 rounded shadow cursor-pointer"
        >
          Logout
        </button>
      </form>
    </section>
  );
}

export default CardProfile;
