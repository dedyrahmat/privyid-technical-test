import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import querystring from "querystring";
import { useSelector } from "react-redux";
import { axiosGlobal } from "../../helpers/global";

function CardEducation({ education }) {
  const [editMode, setEditMode] = useState(false);
  const accessToken = useSelector((state) => state.accessToken);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      school_name: education.school_name,
      graduation_time: editMode.graduation_time,
    },
    validationSchema: Yup.object({
      school_name: Yup.string().required("Nama Institusi tidak boleh kosong."),
      graduation_time: Yup.date().required("Tanggal Lulus tidak boleh kosong."),
    }),
    onSubmit: (values) => updateEducation(values),
  });
  const updateEducation = (values) => {
    const body = querystring.stringify(values);
    const headers = {
      Authorization: accessToken,
    };
    axiosGlobal
      .post("/profile/education", body, { headers })
      .then((response) => {
        if (response.status === 201) {
          console.log("Education Updated");
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  return (
    <div className="bg-white shadow-md rounded border p-4">
      <form onSubmit={formik.handleSubmit} method="post">
        <div className="flex justify-between items-center">
          <h1 className="text-xl font-semibold">Riwayat Pendidikan</h1>
          <span
            onClick={() => setEditMode(!editMode)}
            className="material-icons bg-gray-300 cursor-pointer rounded-full p-2"
          >
            edit
          </span>
        </div>
        <hr className="my-3" />
        <div className="gap-y-1 grid mb-4">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Nama Institusi
          </label>
          {editMode ? (
            <>
              <input
                type="text"
                name="school_name"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.school_name}
                onChange={formik.handleChange}
              />
              {formik.errors.school_name && formik.touched.school_name && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.school_name}
                </p>
              )}
            </>
          ) : (
            <span>{education.school_name || "Tidak tersedia."}</span>
          )}
        </div>
        <div className="gap-y-1 grid mb-2">
          <label htmlFor="name" className="font-bold text-sm text-gray-800">
            Tanggal Lulus
          </label>
          {editMode ? (
            <>
              <input
                type="date"
                name="graduation_time"
                className="outline-none px-2 py-1 bg-gray-200 rounded"
                value={formik.values.graduation_time}
                onChange={formik.handleChange}
              />
              {formik.errors.graduation_time &&
                formik.touched.graduation_time && (
                  <p className="text-red-400 text-sm mx-4 mt-1">
                    {formik.errors.graduation_time}
                  </p>
                )}
            </>
          ) : (
            <span>{education.graduation_time || "Tidak tersedia."}</span>
          )}
        </div>
        {editMode && (
          <button
            type="submit"
            className="bg-blue-400 w-full font-bold outline-none text-white text-center py-2 rounded shadow cursor-pointer"
          >
            Simpan
          </button>
        )}
      </form>
    </div>
  );
}

export default CardEducation;
