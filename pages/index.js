import { useEffect } from "react";
import Head from "next/head";
import { axiosGlobal } from "../helpers/global";
import querystring from "querystring";
import Link from "next/link";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { setAccessToken } from "../store/actionCreator";
import Router from "next/router";

export default function Home() {
  const dispatch = useDispatch();
  const accessToken = useSelector((state) => state.accessToken);

  const formik = useFormik({
    initialValues: {
      phone: "",
      password: "",
      latlong: null,
      device_token: "",
      device_type: 2,
    },
    validationSchema: Yup.object({
      phone: Yup.string()
        .matches(/^[6-9]\d{12}$/, {
          message: "Please enter valid number",
          excludeEmptyString: false,
        })
        .required("Please provide phone number"),
      password: Yup.string().required("Password cannot be blank"),
    }),
    onSubmit: (values) => login(values),
  });

  const login = (values) => {
    const body = querystring.stringify(values);
    axiosGlobal
      .post("/oauth/sign_in", body)
      .then((response) => {
        if (response.status === 201) {
          dispatch(setAccessToken(response.data.data.user.access_token));
          Router.replace("/profile");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    if (accessToken) {
      Router.replace("/profile");
    }
  }, []);

  return (
    <div className="px-2 pt-32">
      <Head>
        <title>Login</title>
      </Head>
      <form onSubmit={formik.handleSubmit}>
        <div className="mx-auto w-full md:w-3/4 lg:w-1/3 py-4 px-4 md:py-8 bg-white shadow-lg rounded-lg grid gap-y-4">
          <h1 className="font-bold text-4xl ml-4 mb-10">Login</h1>
          <div className="gap-y-2 grid">
            <input
              id="phone"
              className="rounded-lg w-full block bg-gray-100 py-3 px-4 outline-none"
              placeholder="Phone"
              value={formik.values.phone}
              onChange={formik.handleChange}
            />
            {formik.errors.phone && formik.touched.phone && (
              <p className="text-red-400 text-sm mx-4 mt-1">
                {formik.errors.phone}
              </p>
            )}
          </div>
          <div className="gap-y-2 grid mb-5">
            <input
              id="password"
              type="password"
              className="rounded-lg w-full block bg-gray-100 py-3 px-4 outline-none"
              placeholder="Password"
              value={formik.values.password}
              onChange={formik.handleChange}
            />
            {formik.errors.password && formik.touched.password && (
              <p className="text-red-400 text-sm mx-4 mt-1">
                {formik.errors.password}
              </p>
            )}
          </div>
          <hr />
          <button
            type="submit"
            className="bg-blue-400 w-full font-bold text-white text-center py-4 rounded shadow cursor-pointer"
          >
            Login
          </button>
          <p className="text-center text-sm">
            Don't have an account yet?{" "}
            <Link href="/register">
              <span className="text-blue-400 hover:underline cursor-pointer">
                Register
              </span>
            </Link>
          </p>
        </div>
      </form>
    </div>
  );
}
