import { useState, useEffect } from "react";
import Head from "next/head";
import { axiosGlobal } from "../helpers/global";
import querystring from "querystring";
import Link from "next/link";
import OtpInput from "react-otp-input";
import { useFormik } from "formik";
import * as Yup from "yup";
import Router from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setAccessToken } from "../store/actionCreator";

function Register() {
  const accessToken = useSelector((state) => state.accessToken);
  const [showOTPInput, setShowOTPInput] = useState(false);
  const [userId, setUserId] = useState("");

  const formik = useFormik({
    initialValues: {
      phone: "",
      password: "",
      country: "",
      latlong: null,
      device_token: "",
      device_type: 2,
    },
    validationSchema: Yup.object({
      phone: Yup.string()
        .matches(/^[6-9]\d{12}$/, {
          message: "Please enter valid number",
          excludeEmptyString: false,
        })
        .required("Please provide phone number"),
      password: Yup.string().required("Password cannot be blank"),
      country: Yup.string().required("Country cannot be blank"),
    }),
    onSubmit: (values) => register(values),
  });

  useEffect(() => {
    if (accessToken) {
      Router.replace("/profile");
    }
  }, []);

  const register = (values) => {
    const body = querystring.stringify(values);
    axiosGlobal
      .post("/register", body)
      .then((response) => {
        if (response.status === 201) {
          setUserId(response.data.data.user.id);
          setShowOTPInput(true);
        }
      })
      .catch((error) => console.warn(error));
  };

  return (
    <div className="px-2 pt-32">
      <Head>
        <title>Register</title>
      </Head>
      {showOTPInput ? (
        <OtpForm userId={userId} phone={formik.values.phone} />
      ) : (
        <form onSubmit={formik.handleSubmit}>
          <div className="mx-auto  w-full md:w-3/4 lg:w-1/3 py-4 px-4 md:py-8 bg-white shadow-lg rounded-lg grid gap-y-4">
            <h1 className="font-bold text-4xl ml-4 mb-10 text-center">
              Registration
            </h1>
            <div>
              <input
                id="phone"
                className="rounded-lg w-full block bg-gray-100 py-3 px-4 outline-none"
                placeholder="Phone"
                value={formik.values.phone}
                onChange={formik.handleChange}
              />
              {formik.errors.phone && formik.touched.phone && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.phone}
                </p>
              )}
            </div>
            <div>
              <input
                id="password"
                type="password"
                className="rounded-lg w-full block bg-gray-100 py-3 px-4 outline-none"
                placeholder="Password"
                value={formik.values.password}
                onChange={formik.handleChange}
              />
              {formik.errors.password && formik.touched.password && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.password}
                </p>
              )}
            </div>
            <div className="mb-5">
              <input
                id="country"
                className="rounded-lg w-full block bg-gray-100 py-3 px-4 outline-none"
                placeholder="Country"
                value={formik.values.country}
                onChange={formik.handleChange}
              />
              {formik.errors.country && formik.touched.country && (
                <p className="text-red-400 text-sm mx-4 mt-1">
                  {formik.errors.country}
                </p>
              )}
            </div>
            <hr />
            <button
              type="submit"
              className="bg-blue-400 w-full outline-none font-bold text-white text-center py-4 rounded shadow cursor-pointer"
            >
              Register
            </button>
            <p className="text-center text-sm">
              Already have an account?{" "}
              <Link href="/">
                <span className="text-blue-400 hover:underline cursor-pointer">
                  Login here
                </span>
              </Link>
            </p>
          </div>
        </form>
      )}
    </div>
  );
}

function OtpForm({ userId, phone }) {
  const [OTP, setOTP] = useState("");
  const dispatch = useDispatch();

  const otpMatch = () => {
    const body = querystring.stringify({
      user_id: userId,
      otp_code: OTP,
    });

    axiosGlobal
      .post("/register/otp/match", body)
      .then((response) => {
        if (response.status === 201) {
          dispatch(setAccessToken(response.data.data.user.access_token));
          Router.replace("/profile");
        }
      })
      .catch((error) => console.warn(error));
  };

  const resendOtp = () => {
    const body = querystring.stringify({
      phone,
    });

    axiosGlobal
      .post("/register/otp/request", body)
      .then((response) => console.log(response.data))
      .catch((error) => console.warn(error));
  };
  return (
    <div className="mx-auto  w-full md:w-3/4 lg:w-1/3 py-4 px-4 md:py-8 bg-white shadow-lg rounded-lg grid gap-y-4">
      <h1 className="font-bold text-4xl ml-4 mb-10 text-center uppercase">
        Otp
      </h1>
      <div className="w-full mb-8">
        <OtpInput
          value={OTP}
          onChange={(otp) => setOTP(otp)}
          numInputs={4}
          separator={"-"}
          inputStyle="w-12 h-12 border rounded mx-4 outline-none"
          containerStyle="w-full flex justify-center text-lg items-center"
          shouldAutoFocus
          isInputNum
        />
      </div>
      <button
        onClick={otpMatch}
        className="bg-blue-400 w-full outline-none font-bold text-white text-center py-3 rounded shadow cursor-pointer"
      >
        Verifikasi
      </button>
      <hr />
      <p
        onClick={resendOtp}
        className="text-center font-bold text-gray-700 cursor-pointer"
      >
        Kirim Ulang Kode OTP
      </p>
    </div>
  );
}

export default Register;
