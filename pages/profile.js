import Head from "next/head";
import { useState, useEffect } from "react";
import Image from "next/image";
import { useSelector } from "react-redux";
import { axiosGlobal } from "../helpers/global";
import CardProfile from "../components/profile/CardProfile";
import CardCareer from "../components/profile/CardCareer";
import CardEducation from "../components/profile/CardEducation";
import Router from "next/router";

function Profile() {
  const [user, setUser] = useState({
    education: {},
    career: {},
    user_pictures: [],
    user_picture: { picture: {} },
    cover_picture: {},
  });
  const [profile, setProfile] = useState({});
  const accessToken = useSelector((state) => state.accessToken);

  useEffect(() => {
    if (!accessToken) {
      Router.replace("/");
    }
    fetchUser();
  }, []);

  const fetchUser = async () => {
    await axiosGlobal
      .get("/oauth/credentials", {
        params: { access_token: accessToken },
      })
      .then((response) => {
        if (response.status === 200) {
          const user = response.data.data.user;
          setUser(user);
          setProfile(user);
        }
      })
      .catch((error) => {});
  };

  const updateAvatar = (e) => {
    const file = e.target.files[0];
    const headers = {
      Authorization: accessToken,
    };
    const formData = new FormData();
    formData.append("image", file);
    axiosGlobal
      .post("/uploads/profile", formData, { headers })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.warn(error));
  };
  const updateCover = (e) => {
    const file = e.target.files[0];
    const headers = {
      Authorization: accessToken,
    };
    const formData = new FormData();
    formData.append("image", file);
    axiosGlobal
      .post("/uploads/cover", formData, { headers })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.warn(error));
  };
  return (
    <div className="px-2 pt-10">
      <Head>
        <title>Profile</title>
      </Head>
      <div className="mx-auto w-2/3 p-4 bg-white shadow rounded">
        <section className="relative">
          <div>
            <div className="absolute -bottom-1/4 left-6 z-10">
              <div className="relative">
                <Image
                  className="rounded-full"
                  src={
                    user.user_picture.picture.url
                      ? user.user_picture.picture.url
                      : "https://place-hold.it/200/60A5FA"
                  }
                  alt="Profile Image"
                  width={200}
                  height={200}
                />
                <div
                  onClick={() => document.getElementById("profile_pic").click()}
                  className="absolute bottom-2 right-0 z-20"
                >
                  <span className="material-icons bg-gray-300 cursor-pointer rounded-full p-3">
                    photo_camera
                  </span>
                </div>
                <input
                  type="file"
                  className="hidden invisible"
                  id="profile_pic"
                  accept="image/x-png,image/gif,image/jpeg"
                  onChange={updateAvatar}
                />
              </div>
            </div>
          </div>
          <div className="relative border rounded">
            <Image
              className="w-full"
              src={
                user.cover_picture.url
                  ? user.cover_picture.url
                  : "https://place-hold.it/1226x300"
              }
              alt="Cover Image"
              width={1226}
              height={300}
              objectFit="cover"
            />
            <div
              onClick={() => document.getElementById("cover_pic").click()}
              className="absolute top-4 right-4"
            >
              <span className="material-icons bg-gray-300 cursor-pointer rounded-full p-3">
                photo_camera
              </span>
            </div>
            <input
              type="file"
              className="hidden invisible"
              id="cover_pic"
              accept="image/x-png,image/gif,image/jpeg"
              onChange={updateCover}
            />
          </div>
          {/* </div> */}
        </section>
        <div className="grid grid-cols-12 gap-6 mt-24">
          <CardProfile user={profile} />
          <section className="col-span-8">
            <div className="grid grid-cols-2 gap-6">
              <CardCareer career={user.career} />
              <CardEducation education={user.education} />
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default Profile;
