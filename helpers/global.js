import Axios from "axios";

const BASE_URL = "http://pretest-qa.dcidev.id/api/v1";

export const axiosGlobal = Axios.create({
  baseURL: BASE_URL,
  responseType: "json",
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
});
